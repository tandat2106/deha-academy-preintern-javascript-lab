
// contructor function
function Validator(options){
    function validate(inputElement,rule){
        var errorMessage = rule.test(inputElement.value);
        var inputError=inputElement.parentElement.querySelector('.message');
        if(errorMessage){
            inputError.innerText=errorMessage;
            inputElement.parentElement.classList.add('invalid');
        }else{
            inputError.innerText="";
            inputElement.parentElement.classList.remove('invalid');
        }
    }
    var formElement = document.querySelector(options.form);
    if(formElement){
        options.rules.forEach(rule => {
            var inputElement = formElement.querySelector(rule.selector);
            
            if(inputElement){
                inputElement.onblur=function(){
                    validate(inputElement,rule);
                }
            }
        });
    }
}

// định nghĩa các rule
//mong muốn: 
//khi có lỗi sẽ báo đỏ ở phần đó và hiện lỗi bên cạnh

//name không được để trống
Validator.isName=function(selector) {
    return {
        selector: selector,
        test : function(value){
            return value.trim() ? undefined :'Vui lòng nhập tên!'
        }
    }
}

//số điện thoại không được để trống và 3 số đầu nằm trong danh sách các đầu số vn
Validator.isPhoneNumber = function(selector) {
    return {
      selector: selector,
      test: function(value) {
        value=value.replace(/\s+/g, '');
        var phoneRegex = /^[0-9]{10}$/; 
        var validPrefixes = [
        //viettel   
        "032", "033", "034", "035", "036", "037", "038", "039", "056", "058", "059", "086",
        // mobiFone
        "070", "077", "078", "079", "084", "090", "093", "094", "095",
        // vinaphone
        "081", "082", "083", "084", "085", "091", "094", "098",
        // vietnamobile
        "063", "064", "065", "066", "067", "068", "069", "089",
        // gmobile
        "096", "097", "098",
        // itelecom
        "087"]; 
        if (value=== '') {
          return 'Vui lòng nhập số điện thoại!';
        } else if (!phoneRegex.test(value)) {
          return 'Số điện thoại không hợp lệ!';
        } else {
          // lấy 2 số đầu của số điện thoại
          var prefix = value.slice(0, 3);
  
          // kiểm tra xem đầu số có nằm trong mảng validPrefixes hay không
          if (!validPrefixes.includes(prefix)) {
            return 'Số điện thoại không hợp lệ!';
          }
        }
  
            return undefined; 
      }
    }
  }
// địa chỉ không được để trống
  Validator.isAddress =function(selector){
    return{
        selector:selector,
        test : function(value){
            return value.trim() ? undefined :'Vui lòng nhập địa chỉ!'
        }
    }
  }

//phải là số, mặc định sẽ là 0 và phải có ít nhất 1 người lớn
  Validator.isValidQuantity = function(selector) {
    return {
      selector: selector,
      test: function(value) {
        var adultInput = document.querySelector('input[name="adult"]');
        var childrenInput = document.querySelector('input[name="children"]');
        var adultValue = parseInt(adultInput.value.trim());
        var childrenValue = parseInt(childrenInput.value.trim());
        var regex = /^[0-9]+$/; 
        if (!regex.test(value.trim())) {
            return 'Vui lòng nhập số người hợp lệ!';
        }
        else if (adultValue === 0){
            return 'Phải có ít nhất một người lớn trong đoàn!';
        }
        else{
            return undefined; 
        }
     }
    }
  }


// phần tử nút submit
var submitBtn = document.getElementById('submit');

// sự kiện khi click vào nút đồng ý
submitBtn.addEventListener('click', function() {
  // biến kiểm tra tính hợp lệ của form  
  var isValid = true; 
  // phần tử hiển thị thông báo
  var messageElement = document.getElementById('submitMessage'); 

  // tạo lại mảng các phương thức
  var rules = [
    Validator.isName('#name'),
    Validator.isPhoneNumber('#phone'),
    Validator.isAddress('#address'),
    Validator.isValidQuantity('#adult'),
    Validator.isValidQuantity('#children')
  ];

  //chạy các phương thức với các phần tử tương ứng
  for (var i = 0; i < rules.length; i++) {
    var inputElement = document.querySelector(rules[i].selector);
    var errorMessage = rules[i].test(inputElement.value);

    if (errorMessage) {
      isValid = false; 
      break; 
    }
  }

  // đổi màu và hiện thị thông báo tương ứng 
  if (isValid) {
    messageElement.innerText = 'Đăng ký thành công!';
    messageElement.style.color = 'green'; 
    messageElement.style.visibility = 'unset';
  } else {
    messageElement.innerText = 'Đăng ký thất bại! Vui lòng nhập lại.';
    messageElement.style.color = 'red'; 
    messageElement.style.visibility = 'unset';
  }
});