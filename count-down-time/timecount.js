/*Note: mình xây dựng lại đồng hộ dưa trên bài mẫu, có tham khảo về cách giảm thời gian mỗi giây cũng như các hàm hiển thị
mình cũng có làm thêm 1 số chức năng như pause và biến stop thành dừng lại và reset thời gian, kiểm tra và khắc phục các lỗi khi nhập sai giá trị */


let h = null; // Giờ
let m = null; // Phút
let s = null; // Giây

let timeout = null; // Timeout

function start(){
if(parseInt(document.getElementById('hNum').value)<0||parseInt(document.getElementById('mNum').value)<0||parseInt(document.getElementById('sNum').value)<0){
        alert('Sai giá trị');
    }
else{

    
   /*LẤY GIÁ TRỊ BAN ĐẦU*/
    if (h === null)
    {
        h = parseInt(document.getElementById('hNum').value);
        m = parseInt(document.getElementById('mNum').value);
        s = parseInt(document.getElementById('sNum').value);
        if(isNaN(h)){
            h=0;
        }
        if(isNaN(s)){
            s=0;
        }
        if(isNaN(m)){
            m=0;
        }
        //đổi giá trị khi nhập sai số giây hoặc phút
        if(s>=60){
            m+=Math.floor(s / 60);
            s=s%60;
        }
        if(m>=60){
            h+=Math.floor(m / 60);
            m=m%60;
        }

    }
    /*CHUYỂN ĐỔI DỮ LIỆU*/
    // Nếu số giây = -1 tức là đã chạy ngược hết số giây, lúc này:
    //  - giảm số phút xuống 1 đơn vị
    //  - thiết lập số giây lại 59
    if (s === -1){
        m -= 1;
        s = 59;
    }

    // Nếu số phút = -1 tức là đã chạy ngược hết số phút, lúc này:
    //  - giảm số giờ xuống 1 đơn vị
    //  - thiết lập số phút lại 59
    if (m === -1){
        h -= 1;
        m = 59;
    }

    // Nếu số giờ = -1 tức là đã hết giờ, lúc này:
    //  - Dừng chương trình
    if (h == -1){
        clearTimeout(timeout);
        alert('Hết giờ');
        h = null; 
        m = null; 
        s = null; 
        return false;

    }

    /*HIỂN THỊ ĐỒNG HỒ*/
    //giờ: nếu số giờ bé hơn 10, sẽ hiện thị thêm số 0 phía trước
    if(h>=10){
        document.getElementById('hour').innerText = h.toString();
    }else{
        document.getElementById('hour').innerText = "0"+h.toString();
    }
    //phút :nếu số phút bé hơn 10, sẽ hiển thị thêm số 0 phía trước
    if(m>=10){
        document.getElementById('minute').innerText = m.toString();
    }else{
        document.getElementById('minute').innerText ="0"+ m.toString();
    }

    //giây: nếu số giây bé hơn 10 sẽ hiển thie5 thêm số 0 phía trước
    if(s>=10){
        document.getElementById('second').innerText = s.toString();
    }else{
        document.getElementById('second').innerText = "0"+s.toString();
    }

    //nếu số giây còn bé hơn 3 thì đổi thành màu đỏ
    var secondElement = document.getElementById('second');
    secondElement.style.color = 'white';
    secondElement.style.textShadow='2vw 2vw 4vw rgba(253, 218, 218, 0.5), -2vw -2vw 4vw rgba(255, 208, 0, 0.5)';

    if (h===0&&m===0&&s <= 3) {
    secondElement.style.color = 'red';
    secondElement.style.textShadow = '2vw 2vw 4vw rgba(253, 50, 50, 0.5),-2vw -2vw 4vw rgba(255, 0, 0, 0.5)';

    }

    /*BƯỚC 1: GIẢM PHÚT XUỐNG 1 GIÂY VÀ GỌI LẠI SAU 1 GIÂY */
    timeout = setTimeout(function(){
        s--;
        start();
    }, 1000);
}
}
function stop(){
    document.getElementById('second').innerText = "00";
    document.getElementById('minute').innerText = "00";
    document.getElementById('hour').innerText = "00";
    clearTimeout(timeout);
    h = null; 
    m = null; 
    s = null; 
}

function pause(){
    clearTimeout(timeout);
}